package br.com.joaopaulosilveira.sorteio.banco;

import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class Evento extends Tabela implements Serializable {
    public String nome;
    public Date dataIni;
    public Date dataFim;
    public boolean atual;

    public Evento() {
    }

    public Evento(String nome, Date dataIni, Date dataFim, boolean atual) {
        this.nome = nome;
        this.dataIni = dataIni;
        this.dataFim = dataFim;
        this.atual = atual;
    }

    @Override
    public String getColecao() {
        return "evento";
    }

    @Override
    public String getId() {
        return null;
    }

    public String getNome() {
        return nome;
    }

    public void obtemEvento() {

        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection(getColecao())
                .whereEqualTo("atual", true)
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        List<DocumentSnapshot> documents = queryDocumentSnapshots.getDocuments();
                        if (documents.size() > 0) {
                            DocumentSnapshot document = documents.get(0);
                            nome = document.get("nome").toString();
                            dataIni = document.getDate("dataIni");
                            dataFim = document.getDate("dataFim");
                        }
                    }
//                })
//                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
//                    @Override
//                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
//                        if (task.isSuccessful()) {
//                            if (task.getResult().size() == 1) {
//                                DocumentSnapshot document;
//                                document = task.getResult().getDocuments().get(0);
//                                nome = document.get("nome").toString();
//                                dataIni = document.getDate("dataIni");
//                                dataFim = document.getDate("dataFim");
//                            }
//                        }
//                    }
                });
    }


}
