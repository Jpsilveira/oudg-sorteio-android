package br.com.joaopaulosilveira.sorteio.telas;


import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.Calendar;
import java.util.List;
import java.util.Random;

import br.com.joaopaulosilveira.sorteio.R;
import br.com.joaopaulosilveira.sorteio.banco.Cadastro;
import br.com.joaopaulosilveira.sorteio.banco.Evento;
import br.com.joaopaulosilveira.sorteio.banco.Premio;
import br.com.joaopaulosilveira.sorteio.banco.Venda;

public class RoletaActivity extends AppCompatActivity implements Animation.AnimationListener {

    boolean blnButtonRotation = true;
    int intNumber = 8;
    int ran = 0;
    long lngDegrees = 0;
    int btStart_visible = View.GONE;
    SharedPreferences sharedPreferences;
    private Cadastro cadastro;
    private Evento evento;
    private long[] produtos;
    private Venda venda;

    ImageView selected, imageRoulette;

    Button btStart;
    Button btResultado;
    TextView textview;
    private ImageView ImagePremio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().addFlags(1024);
        requestWindowFeature(1);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_roleta);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        cadastro = (Cadastro) getIntent().getSerializableExtra("cadastro");
        evento = (Evento) getIntent().getSerializableExtra("evento");
        produtos = getIntent().getLongArrayExtra("produtos");

        btStart = (Button) findViewById(R.id.buttonStart);
        btResultado = (Button) findViewById(R.id.buttonResultado);

        textview = (TextView) findViewById(R.id.t_textview);

        selected = (ImageView) findViewById(R.id.imageSelected);
        imageRoulette = (ImageView) findViewById(R.id.rouletteImage);
        ImagePremio = (ImageView) findViewById(R.id.PremioImage);

        this.sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        setImageRoulette(this.intNumber);
        setImagePremio(0);
        textview.setText("");
        btStart.performClick();

        btStart.setVisibility(btStart_visible);
    }

    @Override
    public void onAnimationStart(Animation animation) {
        this.blnButtonRotation = false;
        btStart.setVisibility(btStart_visible);
        setImageRoulette(this.intNumber);
        setImagePremio(0);
        textview.setText("");
    }

    //     usado para testar os resultados da roleta -> Não apagar!
    public int[] testaResultado() {
        int resultados[] = new int[9];
        int rodadas = 100;
        for (int i = 0; i < rodadas; i++) {
            resultados[resultado(this.intNumber, 0, aleatorio())]++;
        }
        return resultados;
    }

    public int aleatorio() {
        return new Random().nextInt(360) + 5000;
    }

    public int resultado(int qtdPremios, long graus, int aleatorio) {
        if (graus == 0) {
            graus = (graus + ((long) aleatorio)) % 360;
        }

        return (int) (((double) qtdPremios)
                - Math.floor(((double) graus) / (360.0d / ((double) qtdPremios))));
    }

    @Override
    public void onAnimationEnd(Animation animation) {

        final int indice = resultado(this.intNumber, this.lngDegrees, ran);

//        testaResultado();

        Premio premio = new Premio();
        premio.obtemPremio(Long.valueOf(indice))
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        List<DocumentSnapshot> documents = queryDocumentSnapshots.getDocuments();
                        if (documents.size() > 0) {
                            DocumentSnapshot document = documents.get(0);
                            textview.setText("Prêmio\n" + document.get("descricao").toString() + " ");
                            textview.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
                            textview.setTypeface(null, Typeface.BOLD);
                            textview.setBackgroundColor(Color.WHITE);
                            textview.setTextColor(Color.BLUE);
                            textview.setPadding(10, 10, 30, 10);

                            Toast toast = new Toast(getApplication().getApplicationContext());
                            toast.setView(textview);
                            toast.setDuration(Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.RIGHT | Gravity.TOP, 90, 50);

                            btStart.setVisibility(btStart_visible);
                            btResultado.setVisibility(View.VISIBLE);

                            setImagePremio(indice);

                        }
                    }
                });
        this.blnButtonRotation = true;

        Venda venda = new Venda(evento.getNome(), cadastro.getEmail(), Calendar.getInstance().getTime(), (long) indice);
        venda.GravaVenda(produtos);
    }

    @Override
    public void onBackPressed() {
        AbrePrincipal();
    }

    public void AbrePrincipal() {
        Intent intent = new Intent(this, PrincipalActivity.class);
        intent.setFlags(intent.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(intent);
    }

    @Override
    public void onAnimationRepeat(Animation animation) {
    }

    public void onClickButtonRotation(View v) {
        if (this.blnButtonRotation) {

            int ran = aleatorio();

            RotateAnimation rotateAnimation = new RotateAnimation((float) this.lngDegrees, (float)
                    (this.lngDegrees + ((long) ran)), 1, 0.5f, 1, 0.5f);

            this.lngDegrees = (this.lngDegrees + ((long) ran)) % 360;

            rotateAnimation.setDuration((long) ran);
            rotateAnimation.setFillAfter(true);
            rotateAnimation.setInterpolator(new DecelerateInterpolator());
            rotateAnimation.setAnimationListener(this);
            imageRoulette.setAnimation(rotateAnimation);
            imageRoulette.startAnimation(rotateAnimation);
        }
    }

    public void onClickButtonResultado(View v) {
        AbrePrincipal();
    }

    private void setImageRoulette(int myNumber) {
        switch (myNumber) {
            case 1:
                imageRoulette.setImageDrawable(getResources().getDrawable(R.drawable.roulette));
                return;
            case 2:
                imageRoulette.setImageDrawable(getResources().getDrawable(R.drawable.roulette_2));
                return;

            case 3:
                imageRoulette.setImageDrawable(getResources().getDrawable(R.drawable.roulette_3));
                return;
            case 4:
                imageRoulette.setImageDrawable(getResources().getDrawable(R.drawable.roulette_4));
                return;

            case 5:
                imageRoulette.setImageDrawable(getResources().getDrawable(R.drawable.roulette_5));
                return;
            case 6:
                imageRoulette.setImageDrawable(getResources().getDrawable(R.drawable.roulette_6));
                return;

            case 7:
                imageRoulette.setImageDrawable(getResources().getDrawable(R.drawable.roulette_7));
                return;
            case 8:
                imageRoulette.setImageDrawable(getResources().getDrawable(R.drawable.roulette_8));
                return;

            case 9:
                imageRoulette.setImageDrawable(getResources().getDrawable(R.drawable.roulette_9));
                return;
            case 10:
                imageRoulette.setImageDrawable(getResources().getDrawable(R.drawable.roulette_10));
                return;

        }
    }

    private void setImagePremio(int myNumber) {
        switch (myNumber) {
            case 0:
                ImagePremio.setImageDrawable(getResources().getDrawable(R.drawable.premio));
                return;
            case 1:
                ImagePremio.setImageDrawable(getResources().getDrawable(R.drawable.premio_1));
                return;
            case 2:
                ImagePremio.setImageDrawable(getResources().getDrawable(R.drawable.premio_2));
                return;

            case 3:
                ImagePremio.setImageDrawable(getResources().getDrawable(R.drawable.premio_3));
                return;
            case 4:
                ImagePremio.setImageDrawable(getResources().getDrawable(R.drawable.premio_4));
                return;

            case 5:
                ImagePremio.setImageDrawable(getResources().getDrawable(R.drawable.premio_5));
                return;
            case 6:
                ImagePremio.setImageDrawable(getResources().getDrawable(R.drawable.premio_6));
                return;

            case 7:
                ImagePremio.setImageDrawable(getResources().getDrawable(R.drawable.premio_7));
                return;
            case 8:
                ImagePremio.setImageDrawable(getResources().getDrawable(R.drawable.premio_8));
                return;

        }
    }

}
