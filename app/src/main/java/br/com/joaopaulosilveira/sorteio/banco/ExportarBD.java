package br.com.joaopaulosilveira.sorteio.banco;

import android.content.Context;
import android.os.Environment;

import com.google.firebase.Timestamp;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Date;

import br.com.joaopaulosilveira.sorteio.ouvidores.SingleMediaScanner;

public class ExportarBD {

    public boolean exportaColecaoArquivo(QuerySnapshot querysnapshot, String nomeColecao, Context context) {
        if (Environment.getExternalStorageState().equalsIgnoreCase("mounted"))//Check if Device Storage is present
        {
            try {
                File root = new File(Environment.getExternalStorageDirectory(), "Sorteio");//You might want to change this to the name of your app. (This is a folder that will be created to store all of your txt files)
                if (!root.exists()) {
                    root.mkdirs();
                }
                File arquivoTxt = new File(root, nomeColecao + ".txt"); //You might want to change the filename
                if (arquivoTxt.exists()) {
                    arquivoTxt.delete();
                    new SingleMediaScanner(context, arquivoTxt);
                }
                OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(arquivoTxt, true), "windows-1252");

                exportaLinhasColecao(querysnapshot, writer);

                writer.flush();
                writer.close();
                new SingleMediaScanner(context, arquivoTxt);
            } catch (IOException e) {
                e.getMessage();
                return false;
            }

        }
        return true;
    }

    public boolean exportaLinhasColecao(QuerySnapshot querysnapshot, OutputStreamWriter writer) {
        String text = new String();
        DocumentSnapshot document;

        text = querysnapshot.getDocuments().get(0).getData().keySet().toString();
        text = text.replace("[", "").replace("]", "").replace(",", "\t");
        try {
            writer.append(text + "\r\n");
            text = "";
        } catch (IOException e) {
            return false;
        }

        for (int i = 0; i < querysnapshot.size(); i++) {

            Object[] array;// = new Object[querysnapshot.getDocuments().get(i).getData().size() - 1];
            array = querysnapshot.getDocuments().get(i).getData().values().toArray();
            for (int x = 0; x < array.length; x++) {
                if (array[x] != null) {
                    if (array[x].getClass() == Timestamp.class) {
                        java.util.Date date = new Date();
                        date = ((Timestamp) array[x]).toDate();
                        array[x] = (String) new java.text.SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(date);
                    }
                }
                text = text + array[x] + "\t";
            }

            text = text.replace("[", "").replace("]", "").replace(",", "\t");
            try {
                writer.append(text + "\r\n");
                text = "";
            } catch (IOException e) {
                return false;
            }
        }
        return true;
    }
}
