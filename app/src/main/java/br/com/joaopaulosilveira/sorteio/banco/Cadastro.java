package br.com.joaopaulosilveira.sorteio.banco;

import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

import java.io.Serializable;

public class Cadastro extends Tabela implements Serializable {
    public String email;
    public String emailcheck;
    public String nome;
    public String whatsapp;

    public Cadastro() {
    }

    ;

    public Cadastro(String email, String emailcheck, String nome, String whatsapp) {
        this.email = email;
        this.emailcheck = emailcheck;
        this.nome = nome;
        this.whatsapp = whatsapp;
    }

    public String getEmail() {
        return email;
    }

    @Override
    public String getColecao() {
        return "cadastro";
    }

    @Override
    public String getId() {
        return email;
    }

    public Query PesquisaEmail(String id) {

        FirebaseFirestore db = FirebaseFirestore.getInstance();
        return db.collection(getColecao())
                .whereEqualTo("email", id);
    }

}
