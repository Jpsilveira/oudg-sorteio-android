package br.com.joaopaulosilveira.sorteio.banco;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.List;

public class Premio extends Tabela {
    public long codigo;
    public String descricao;

    public Premio() {
    }

    public Premio(long codigo, String descricao) {
        this.codigo = codigo;
        this.descricao = descricao;
    }

    @Override
    public String getColecao() {
        return "premio";
    }

    @Override
    public String getId() {
        return null;
    }

    public String getDescricao() {
        return descricao;
    }

    public Task<QuerySnapshot> obtemPremio(Long premio) {

        FirebaseFirestore db = FirebaseFirestore.getInstance();
        return db.collection(getColecao())
                .whereEqualTo("codigo", premio)
                .get();
    };
//                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
//                    @Override
//                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
//                        List<DocumentSnapshot> documents = queryDocumentSnapshots.getDocuments();
//                        if (documents.size() > 0) {
//                            DocumentSnapshot document = documents.get(0);
//                            descricao = document.get("descricao").toString();
//                            codigo = document.getLong("codigo");
//                        }
//                    }
////                })
////                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
////                    @Override
////                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
////                        if (task.isSuccessful()) {
////                            if (task.getResult().size() == 1) {
////                                DocumentSnapshot document;
////                                document = task.getResult().getDocuments().get(0);
////                                nome = document.get("nome").toString();
////                                dataIni = document.getDate("dataIni");
////                                dataFim = document.getDate("dataFim");
////                            }
////                        }
////                    }
//                });
//    }

}
