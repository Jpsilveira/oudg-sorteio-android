package br.com.joaopaulosilveira.sorteio.ouvidores;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.Toast;

import br.com.joaopaulosilveira.sorteio.R;
import br.com.joaopaulosilveira.sorteio.banco.Produto;

public class ProdutoViewHolder extends RecyclerView.ViewHolder {
    private CheckBox cbProduto;

    public ProdutoViewHolder(@NonNull View itemView) {
        super(itemView);
        cbProduto = itemView.findViewById(R.id.cb_produto);
    }

    public void setProduto(Produto produto) {
        cbProduto.setTag(produto.getCodigo());
        cbProduto.setText(produto.getDescricao());
    }

    public void setOnClickListener(View.OnClickListener listener) {
        cbProduto.setOnClickListener(listener);
    }
}
