package br.com.joaopaulosilveira.sorteio.banco;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;

import java.io.Serializable;
import java.util.Date;

public class Venda extends Tabela implements Serializable {
    public String evento;
    public String email;
    public Date horario;
    public long premio;

    public Venda() {
    }

    public Venda(String evento, String email, Date horario, long premio) {
        this.evento = evento;
        this.email = email;
        this.horario = horario;
        this.premio = premio;
    }

    @Override
    public String getColecao() {
        return "venda";
    }

    @Override
    public String getId() {
        return null;
    }

    public void GravaVenda(final long[] produtos) {

        inserir()
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        if (produtos.length > 0) {
                            for (int i = 0; i < produtos.length; i++) {
                                if (produtos[i] != 0) {
                                    VendaItem vendaItem = new VendaItem(documentReference.getId(), produtos[i]);
                                    vendaItem.inserir()
                                            .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                                                @Override
                                                public void onSuccess(DocumentReference documentReference) {
                                                }
                                            });
                                }
                            }
                        }
                    }
                });
    }
}

