package br.com.joaopaulosilveira.sorteio.banco;

import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

public abstract class Tabela {
    public abstract String getColecao();

    public abstract String getId();

    private FirebaseFirestore db = FirebaseFirestore.getInstance();

    public Map<String, Object> asMap() {

        Map<String, Object> map = new HashMap<>();

        for (Field field : getClass().getFields()) {
            try {
                if (field.getName() != "$change" && field.getName() != "serialVersionUID") {
                    map.put(field.getName(), field.get(this));
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }

        return map;
    }

    public Task<DocumentReference> inserir() {
        return db.collection(getColecao()).add(asMap());
    }

    public Task<QuerySnapshot> listar2() {
        return db.collection(getColecao()).get();
    }

    public CollectionReference listar() {
        return db.collection(getColecao());
    }

    public Task<Void> salvar() {
        return db.collection(getColecao()).document(getId()).set(asMap());
    }

}