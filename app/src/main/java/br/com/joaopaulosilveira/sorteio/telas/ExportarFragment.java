package br.com.joaopaulosilveira.sorteio.telas;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;

import br.com.joaopaulosilveira.sorteio.R;
import br.com.joaopaulosilveira.sorteio.banco.ExportarBD;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link ExportarFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ExportarFragment extends Fragment {
    private Button btExportar;
    private ProgressBar spinner;

    public ExportarFragment() {
        // Required empty public constructor
    }

    public static ExportarFragment newInstance() {
        ExportarFragment fragment = new ExportarFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_exportar, container, false);

        btExportar = view.findViewById(R.id.bt_exportar);

        btExportar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                exportTxt();
            }
        });

        spinner = view.findViewById(R.id.progressBar1);

        return view;
    }

    public void exportTxt() {

        final String colecoes[] = {
                "cadastro",
                "evento",
                "premio",
                "produto",
                "venda",
                "venda_item"};

        FirebaseFirestore db = FirebaseFirestore.getInstance();


        for (int i = 0; i < colecoes.length; i++) {
            final int finalI = i;
            spinner.setVisibility(View.VISIBLE);
            db.collection(colecoes[i])
                    .addSnapshotListener(new EventListener<QuerySnapshot>() {
                        @Override
                        public void onEvent(@Nullable QuerySnapshot snapshots,
                                            @Nullable FirebaseFirestoreException e) {

                            ExportarBD exportarBD = new ExportarBD();
                            for (DocumentChange dc : snapshots.getDocumentChanges()) {
                                if (!exportarBD.exportaColecaoArquivo(snapshots, colecoes[finalI], getContext())) {
                                    Toast.makeText(getContext(), "Erro ao exportar base de dados.("+colecoes[finalI]+")", Toast.LENGTH_SHORT).show();
                                }
                            }
                            spinner.setVisibility(View.GONE);
                            Toast.makeText(getContext(), "Exportação da tabela "+colecoes[finalI]+" concluída com sucesso.", Toast.LENGTH_SHORT).show();
                        }
                    });
        }
    }
}
