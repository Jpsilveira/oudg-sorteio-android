package br.com.joaopaulosilveira.sorteio.banco;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;

public class VendaItem extends Tabela {
    public String vendaId;
    public long produtoCodigo;

    @Override
    public String getColecao() {
        return "venda_item";
    }

    @Override
    public String getId() {
        return null;
    }

    public VendaItem() {
    }

    public VendaItem(String vendaId, long produtoCodigo) {
        this.vendaId = vendaId;
        this.produtoCodigo = produtoCodigo;
    }

    public void GravaItensVenda(String vendaId) {
        inserir()
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                    }
                });
    }
}
