package br.com.joaopaulosilveira.sorteio.ouvidores;

import java.util.Calendar;
import java.util.Date;

public class Singleton {

    private static final Singleton instance = new Singleton();

    private Singleton() {
    }

    public static Singleton getInstance() {
        return instance;
    }

    private String emailAnterior;
    private Date DataVendaAnterior;

    public String getEmailAnterior() {
        return emailAnterior;
    }

    public void setEmailAnterior(String emailAnterior) {
        this.emailAnterior = emailAnterior;
    }

    public Date getDataVendaAnterior() {
        return DataVendaAnterior;
    }

    public void setDataVendaAnterior(Date dataVendaAnterior) {
        DataVendaAnterior = dataVendaAnterior;
    }

    public Date getDataVendaNova() {
        Calendar data = Calendar.getInstance();
        if (DataVendaAnterior != null) {
            data.setTime(DataVendaAnterior);
            data.add(Calendar.MINUTE, 5);
        }
        return data.getTime();
    }


}
