package br.com.joaopaulosilveira.sorteio.banco;

public class Produto extends Tabela {
    public long codigo;
    public String descricao;

    public Produto() {
    }

    public Produto(long codigo, String descricao) {
        this.codigo = codigo;
        this.descricao = descricao;
    }

    @Override
    public String getColecao() {
        return "produto";
    }

    @Override
    public String getId() {
        return null;
    }

    public long getCodigo() {
        return codigo;
    }

    public String getDescricao() {
        return descricao;
    }
}
