package br.com.joaopaulosilveira.sorteio.ouvidores;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import br.com.joaopaulosilveira.sorteio.R;
import br.com.joaopaulosilveira.sorteio.banco.Produto;
import br.com.joaopaulosilveira.sorteio.telas.CadastroFragment;

public class ProdutosAdapter extends RecyclerView.Adapter<ProdutoViewHolder> {
    private List<Produto> produtos;
    private List<Long> selecionados;
    private CadastroFragment fragment;

    public ProdutosAdapter(CadastroFragment fragment, List<Produto> produtos) {
        this.produtos = produtos;
        this.fragment = fragment;
        selecionados = new ArrayList<>();
    }

    @NonNull
    @Override
    public ProdutoViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_produto, viewGroup, false);
        return new ProdutoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProdutoViewHolder produtoViewHolder, int i) {
        produtoViewHolder.setProduto(produtos.get(i));
        produtoViewHolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                long tag = Long.parseLong(v.getTag().toString());
                int index = selecionados.indexOf(tag);
                if (index != -1) {
                    // selecionado, remover
                    selecionados.remove(index);
                } else {
                    // adicionando
                    selecionados.add(tag);
                }

                fragment.validaChecks();
            }
        });
    }

    @Override
    public int getItemCount() {
        return produtos.size();
    }

    public List<Long> getSelecionados() {
        return selecionados;
    }

    public boolean selecionouAlgum() {
        return selecionados.size() > 0;
    }
}
