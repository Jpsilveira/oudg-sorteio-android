package br.com.joaopaulosilveira.sorteio.telas;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import br.com.joaopaulosilveira.sorteio.R;
import br.com.joaopaulosilveira.sorteio.banco.Cadastro;
import br.com.joaopaulosilveira.sorteio.banco.Evento;
import br.com.joaopaulosilveira.sorteio.banco.Premio;
import br.com.joaopaulosilveira.sorteio.banco.Venda;
import br.com.joaopaulosilveira.sorteio.banco.VendaItem;

public class PrincipalActivity extends AppCompatActivity {

    private static final String EXPORTAR_TAG = "ExpFrag";
    private static final String CADFRAG_TAG = "CadFrag";
    private static final int REQUEST_LOCATION = 2;
    private static final int REQUEST_CODE_WRITE_EXTERNAL_STORAGE_PERMISSION = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new CadastroFragment(), CADFRAG_TAG)
                    .commitAllowingStateLoss();
        }


        // Check whether this app has write external storage permission or not.
        int writeExternalStoragePermission = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        // If do not grant write external storage permission.
        if (writeExternalStoragePermission != PackageManager.PERMISSION_GRANTED) {
            // Request user to grant write external storage permission.
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CODE_WRITE_EXTERNAL_STORAGE_PERMISSION);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menus, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        replaceForm(item.getItemId());

        return true;
    }

    private void replaceForm(@IdRes int menuId) {
//        Fragment fragment = null;
        String tag = null;

//        getSupportFragmentManager().popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        switch (menuId) {
            case R.id.ExpFrag:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, new ExportarFragment(), CADFRAG_TAG)
                        .commitAllowingStateLoss();
//                fragment = new ExportarFragment();
                tag = EXPORTAR_TAG;
                break;

            case R.id.CadFrag:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, new CadastroFragment(), CADFRAG_TAG)
                        .commitAllowingStateLoss();
//                fragment = new CadastroFragment();
                tag = CADFRAG_TAG;
                break;

            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }


}
