package br.com.joaopaulosilveira.sorteio.telas;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import br.com.joaopaulosilveira.sorteio.R;
import br.com.joaopaulosilveira.sorteio.banco.Cadastro;
import br.com.joaopaulosilveira.sorteio.banco.Evento;
import br.com.joaopaulosilveira.sorteio.banco.Produto;
import br.com.joaopaulosilveira.sorteio.ouvidores.ProdutosAdapter;
import br.com.joaopaulosilveira.sorteio.ouvidores.Singleton;

//import android.support.v4.app.Fragment;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link CadastroFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CadastroFragment extends Fragment {
    private EditText etNome;
    private EditText etEmail;
    private EditText etEmailConfirmacao;
    private EditText etWhatsapp;
    private TextView tvProdutos;
    private Button btSalvar;
    private RecyclerView rvProdutos;
    private ProdutosAdapter produtosAdapter;
    private Evento evento;
    private ProgressBar spinner;

    private boolean isNomeValid = false;
    private boolean isEmailValid = false;
    private boolean isEmailConfirmacaoValid = false;
    private boolean isWhatsappValid = true;
    private boolean isProdutosValid = false;

    public void updateSalvarButton() {
        btSalvar.setEnabled(isNomeValid && isEmailValid && isEmailConfirmacaoValid && isWhatsappValid && isProdutosValid);
    }

    public CadastroFragment() {
        // Required empty public constructor
    }

    public static CadastroFragment newInstance() {
        CadastroFragment fragment = new CadastroFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_cadastro, container, false);

        evento = new Evento();
        evento.obtemEvento();

        etEmail = view.findViewById(R.id.et_email);
        etEmailConfirmacao = view.findViewById(R.id.et_email_confirmacao);
        etNome = view.findViewById(R.id.et_nome);
        etWhatsapp = view.findViewById(R.id.et_whatsapp);
        tvProdutos = view.findViewById(R.id.tv_produtos);
        rvProdutos = view.findViewById(R.id.rv_produtos);
        btSalvar = view.findViewById(R.id.bt_salvar);
        spinner = view.findViewById(R.id.progressBar1);

        carregaProdutos();

        etEmail.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                updateSalvarButton();

                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    // consultar email
                    Cadastro cadastro = new Cadastro();
                    cadastro.PesquisaEmail(etEmail.getText().toString())
                            .addSnapshotListener(new EventListener<QuerySnapshot>() {
                                @Override
                                public void onEvent(@Nullable QuerySnapshot snapshots,
                                                    @Nullable FirebaseFirestoreException e) {

                                    for (DocumentChange dc : snapshots.getDocumentChanges()) {
                                        etEmailConfirmacao.setText(dc.getDocument().getData().get("emailcheck").toString());
                                        etNome.setText(dc.getDocument().getData().get("nome").toString());
                                        etWhatsapp.setText(dc.getDocument().getData().get("whatsapp").toString());
                                        etEmail.setEnabled(false);
                                        etEmailConfirmacao.setVisibility(View.GONE);
                                        isNomeValid = true;
                                        isWhatsappValid = true;
                                        isEmailValid = true;
                                        isEmailConfirmacaoValid = true;

                                    }
                                }
                            });

                    InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

                    return true;
                }
                return false;
            }
        });

        etNome.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    etNome.setText(etNome.getText().toString().trim());
                    etNome.setError(null);
                    isNomeValid = false;
                    if (etNome.length() > 5) {
                        isNomeValid = Pattern.compile("^(?!.{52,})[a-zA-Z\\.\\'\\-\\S]{2,50}(?: [a-zA-Z\\.\\'\\-\\S]{2,50})+$").matcher(etNome.getText().toString()).matches();
                    }
                    if (!isNomeValid) etNome.setError("Informe nome e sobrenome.");
                    updateSalvarButton();
                }
            }
        });

        etEmail.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    validaEmails();
                }
            }
        });

        etEmailConfirmacao.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    validaEmails();
                }
            }
        });

        etWhatsapp.addTextChangedListener(new TextWatcher() {
            int beforeLength;
            String telefone;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                beforeLength = etWhatsapp.length();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int digits = etWhatsapp.getText().toString().length();
                if (beforeLength < digits && (digits == 1)) {
                    etWhatsapp.setText(null);
                    etWhatsapp.append("(" + s);
                } else if (beforeLength < digits && (digits == 3)) {
                    etWhatsapp.append(")");
                } else if (beforeLength < digits && (digits == 9)) {
                    etWhatsapp.append("-");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        etWhatsapp.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    etWhatsapp.setError(null);
                    isWhatsappValid = true;
                    if (etWhatsapp.length() > 0) {
                        if (etWhatsapp.length() < 13) {
                            isWhatsappValid = false;
                        } else {
                            isWhatsappValid = Patterns.PHONE.matcher(etWhatsapp.getText().toString()).matches();
                        }
                    }
                    if (!isWhatsappValid)
                        etWhatsapp.setError("Informe um número de celular válido ou deixe em branco.");
                    updateSalvarButton();
                }
            }
        });

        btSalvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                salvar();
            }
        });

        return view;
    }

    public void carregaProdutos() {
        spinner.setVisibility(View.VISIBLE);
        new Produto()
                .listar()
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot snapshots,
                                        @Nullable FirebaseFirestoreException e) {

                        List<Produto> produtos = new ArrayList<>();

                        for (DocumentChange dc : snapshots.getDocumentChanges()) {
                            Produto produto = new Produto(Long.parseLong(dc.getDocument().getData().get("codigo").toString()), dc.getDocument().getData().get("descricao").toString());
                            produtos.add(produto);
                        }
                        produtosAdapter = new ProdutosAdapter(CadastroFragment.this, produtos);
                        rvProdutos.setAdapter(produtosAdapter);
                        rvProdutos.setLayoutManager(new GridLayoutManager(getContext(), 2));
                        spinner.setVisibility(View.GONE);
                    }
                });
    }

    public void validaEmails() {
        isEmailValid = true;
        isEmailConfirmacaoValid = true;

        etEmail.setError(null);
        etEmailConfirmacao.setError(null);

        if (etEmail.length() > 0 && etEmailConfirmacao.length() > 0) {
            //isEmailConfirmacaoValid = s.toString().equals(etEmail.getText().toString());
            if (etEmail.getText().toString() == null) {
                isEmailValid = false;
            }

            if (etEmailConfirmacao.getText().toString() == null) {
                isEmailConfirmacaoValid = false;
            }

            isEmailValid = Patterns.EMAIL_ADDRESS.matcher(etEmail.getText().toString()).matches();
            isEmailConfirmacaoValid = Patterns.EMAIL_ADDRESS.matcher(etEmailConfirmacao.getText().toString()).matches();
        }
        if (isEmailConfirmacaoValid) {
            isEmailConfirmacaoValid = etEmailConfirmacao.getText().toString().equals(etEmail.getText().toString());
        }
        if (!isEmailConfirmacaoValid)
            etEmailConfirmacao.setError("O email não é válido ou está diferente do informado previamente.");
        if (!isEmailValid)
            etEmail.setError("Informe um email válido ou está diferente do informado na confirmação");
        updateSalvarButton();
    }

    public void validaChecks() {
        tvProdutos.setError(null);
        isProdutosValid = true;
        if (!produtosAdapter.selecionouAlgum()) {
            isProdutosValid = false;
            tvProdutos.setError("Selecione ao menos um dos produtos.");
            Toast.makeText(getContext(), "Selecione ao menos um dos produtos.", Toast.LENGTH_SHORT).show();
        }
        updateSalvarButton();
    }

    public void salvar() {
        final Cadastro cadastro = new Cadastro(
                etEmail.getText().toString(),
                etEmailConfirmacao.getText().toString(),
                etNome.getText().toString(),
                etWhatsapp.getText().toString());

        cadastro.salvar();

        Intent intent = new Intent(getActivity(), RoletaActivity.class);
        intent.setFlags(intent.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY);
        intent.putExtra("cadastro", cadastro);
        intent.putExtra("evento", evento);

        List<Long> selecionados = produtosAdapter.getSelecionados();
        long[] selecionadosDeVerdade = new long[selecionados.size()];
        for (int i = 0; i < selecionados.size(); i++) {
            selecionadosDeVerdade[i] = selecionados.get(i);
        }

        intent.putExtra("produtos", selecionadosDeVerdade);

        Date DataAtual = Calendar.getInstance().getTime();
        String emailAtual = cadastro.getEmail();

        String emailAnterior;
        Date DataVendaAnterior;

        DataVendaAnterior = Singleton.getInstance().getDataVendaNova();
        emailAnterior = Singleton.getInstance().getEmailAnterior();

        if (emailAnterior != null && emailAtual != null && emailAnterior.equals(emailAtual)) {
            if (DataAtual.compareTo(DataVendaAnterior) < 0) {
                Toast.makeText(getContext(), "Nova compra efetuada em menos de 5 minutos. Por favor aguarde.", Toast.LENGTH_LONG).show();
                return;
            }
        }
        Singleton.getInstance().setEmailAnterior(cadastro.getEmail());
        Singleton.getInstance().setDataVendaAnterior(Calendar.getInstance().getTime());

        getActivity().finishAffinity();
        startActivity(intent);
    }
}
